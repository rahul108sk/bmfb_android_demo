from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import os, time
import unittest


class TestBMFBonAppium(unittest.TestCase):
    def setUp(self):
        # আগে থেকে অ্যাপটি ডিভাইসে ইন্সটল থাকলে তা আনইন্সটল করে দেবো  
        os.system("adb -e uninstall com.example.bmfb")
        # ইমুলেটরের ক্ষেত্রে adb -e uninstall com.example.bmfb
        desired_caps = {}
        desired_caps["platformName"] = "Android"
        desired_caps["platformVersion"] = "10.0"
        desired_caps["app"] = os.getcwd()+ '/app_files/' + "BMFB.apk" 
        desired_caps["appPackage"] = "com.example.bmfb"
        desired_caps["deviceName"] = "Huawei Y6 II"
        # desired_caps["appWaitActivity"] = "com.example.bmfb.SplashScreenActivity"
        desired_caps["name"] = "BMFB App testing"
        desired_caps["appiumVersion"] = "1.15.1"
        self.driver = webdriver.Remote("http://0.0.0.0:4723/wd/hub", desired_caps)
        # os.system("adb -e shell pm grant com.example.bmfb android.permission.READ_CONTACTS")

    def test_bmfb(self):
        os.system("adb -e shell am start com.example.bmfb/.MainActivity")
        # ইমুলেটরের ক্ষেত্রে adb -e shell am start com.example.bmfb/.MainActivity
        time.sleep(2)
        print("App Installed on device Successfully!!\n")
        print("T1: We will check for the text 'Search Items Here' ")
        t1 = self.driver.find_element_by_id("search_header")
        self.assertEqual(t1.text,"Search Items Here")
        print("T1 passed: Text Matched!!\n")
        print("T2: Enter text 'Test Automation on Appium' \n")
        t2 = self.driver.find_element_by_id("search_input")
        t2.send_keys("Test Automation on Appium\n")
        self.driver.hide_keyboard()
        print("T2 passed: Text Entered Successfully!!\n")
        time.sleep(2)
        print("T3: Open the Navigation Drawer\n")
        self.driver.find_element_by_accessibility_id("Open navigation drawer").click()
        print("T3 passed")
        time.sleep(2)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestBMFBonAppium)
    unittest.TextTestRunner(verbosity=2).run(suite)
