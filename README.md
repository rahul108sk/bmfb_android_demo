[![pipeline status](https://gitlab.com/rahul108sk/bmfb_android_demo/badges/master/pipeline.svg)](https://gitlab.com/rahul108sk/bmfb_android_demo/commits/master)
[![coverage report](https://gitlab.com/rahul108sk/bmfb_android_demo/badges/master/coverage.svg)](https://gitlab.com/rahul108sk/bmfb_android_demo/commits/master)

# Project Name
Bring Me Food Buddy


# Required
* Openjdk [Version "1.8.0_222"]
* AndroidSdk [Version 26.1.1]
* Appium


# Gitlab CI config
* **Executor:** Shell

In order to use shell, we need to first [install gitlab-runner](https://docs.gitlab.com/runner/install/linux-manually.html), I have installed it on my os manually.

Next we need to [register on the gitlab](https://docs.gitlab.com/runner/register/), choosed shell as executor


* **OS:** Ubuntu 18.04
