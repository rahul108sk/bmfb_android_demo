# Issues

* images with png format

```sh
Execution failed for task ':app:mergeReleaseResources'.
> Multiple task action failures occurred:
   > A failure occurred while executing com.android.build.gradle.internal.tasks.Workers$ActionFacade
      > Android resource compilation failed
        /home/gitlab-runner_1/builds/JBKWvjzo/0/rahul108sk/bmfb_android_demo/app/src/main/res/drawable-v21/food_3.png: AAPT: error: failed to read PNG signature: file does not start with PNG signature.
```
 if occurs then replace the images with jpg formatted pic's

 otherwise follow this instruction from [stackoverflow](https://stackoverflow.com/questions/51220827/appmergereleaseresources-exception-when-making-release-build)

```sh
If its due to the PNG files present in the Project, then add the below in gradle.

aaptOptions {  
    cruncherEnabled = false  
}
```


* Execution failed for task ':app:lint'

```sh
Lint found errors in the project; aborting build.

  Fix the issues identified by lint, or add the following to your build script to proceed with errors:
  ...
  android {
      lintOptions {
          abortOnError false
      }
  }
  ...

  The first 3 errors (out of 5) were:
/home/gitlab-runner_2/builds/JBKWvjzo/0/rahul108sk/bmfb_android_demo/app/build.gradle:30: Error: Version 28 (intended for Android Pie and below) is the last version of the legacy support library, so we recommend that you migrate to AndroidX libraries when using Android Q and moving forward. The IDE can help with this: Refactor > Migrate to AndroidX... [GradleCompatible]
    implementation 'com.android.support:cardview-v7:28.0.0'
```
for this go to *app > build.gradle*   ----then replace the dependency of cardview with this...

```sh
dependencies {
    implementation 'androidx.cardview:cardview:1.0.0'
```
